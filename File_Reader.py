# coding=utf-8
import os
try:
    import tkinter as tk  # import tk package
except ImportError:
    print('###this code has some problem need to fix, please ues python3###')
    # import Tkinter as tk

from tkinter import *
import tkinter.filedialog
from tkinter import scrolledtext

# 全域變數
file_path = ''


# Set main frame
window = tk.Tk()  # 宣告 windows 為 Tk()類型
window.title('File Reader ver0.1')
window.geometry('700x300')  # (長 * 寬)

text_view = tk.Text(window, width=100, height=5, wrap=tk.WORD)
text_state = tk.Text(window, width=100, height=1, wrap=tk.WORD)
text_find = tk.Entry(window, show=None)
text_target = tk.Text(window, width=30, height=30, wrap=tk.WORD)

text_view.insert(tk.END, '預覽視窗')
text_state.insert(tk.END, '檔案路徑')

def read_all_click():
    global file_path
    global text0
    if file_path == '':
        text_state.insert(tk.END,'Select file first. \n')
    else:
        try:
            file0 = open(file_path, 'r', encoding='utf8')
            f0 = file0.read()
            subwindow0 = tk.Toplevel()
            text0 = scrolledtext.ScrolledText(subwindow0, width=70, height=99, wrap=tk.WORD)
            text0.insert(tk.END, f0)
            text0.pack(fill=tk.BOTH)
            file0.close()
        except FileNotFoundError:
            text_state.insert(tk.END,'Read Error')
        except UnicodeDecodeError:
            text_state.insert(tk.END, 'UnicodeDecodeError')
            file0.close()

def load_file_click():
    global file_path
    global text0
    # file_path = tkinter.filedialog.askopenfilename(filetypes=(("text File", "*.txt"),))
    file_path = tkinter.filedialog.askopenfilename(filetypes=(("text File", "*.txt"), ("All", "*.*")))
    if file_path != '':
        text_state.insert(tk.END, file_path)
        file0 = open(file_path, 'r', encoding='utf8')
        for i in range(5):
            text_view.insert('insert', file0.readline())
    else:
        text_state.insert(tk.END, 'No file Select.\n')

def find_line_click():
    text_target.delete('1.0','end')
    findname = text_find.get()
    file0 = open(file_path, 'r', encoding='utf8')
    for line in file0:
        find_one_line = line.find(findname)
        if find_one_line != -1:
            input = line
            text_target.insert('insert', input)
            text_target.pack()
            break
        else:
            text_state.insert(tk.END, 'Target found.\n')


button_read = tk.Button(window, text='選擇檔案', font=('Arial', 12), width=10, height=1, command=load_file_click)
button_load = tk.Button(window, text='顯示全部內容', font=('Arial', 12), width=10, height=1, command=read_all_click)
button_find = tk.Button(window, text='搜尋', font=('Arial', 12), width=10, height=1, command=find_line_click)
text_view.pack()
text_state.pack()
button_read.pack()
button_load.pack()
text_find.pack()
button_find.pack()

window.mainloop()



# text0.pack()


# 主視窗迴圈顯示
